*** Settings ***
Library    Selenium2Library
Library    ExcelLibrary
Library    String
*** Variables ***

${url}    https://www.google.com/
${btn}    //*[@class="FPdoLc VlcLAe"]//*[@value="ค้นหาด้วย Google"]
*** Keywords ***

Input Username
    [Arguments]    ${xpath}    ${username}
    Input Text    ${xpath}    ${username}
*** Test Cases ***
TC01
    [Tags]    positive    Negative
    Open Browser    https://www.google.com/    gc

TC02
    [Tags]    positive    Negative
    Open Browser    https://www.google.com/    gc
    Maximize Browser Window
    Page Should Contain Element    //*[@name="q"]
    # Input Text    //*[@name="q"]    test
    Input Username    //*[@name="q"]    test 

TC03
    [Tags]    Negative
    Open Browser    ${url}    gc
    Maximize Browser Window
    Page Should Contain Element    //*[@name="q"]
    # Input Text    //*[@name="q"]    test
    Input Username    //*[@name="q"]    test
    Click Element    //*[@id="lga"]
    Click Element    ${btn}
    Close Browser



TC04
    Open Browser    https://robotframework.org/    gc
    ${txtrobot}    Get Text    //*[@class="main-header"]
    Log To Console    ${txtrobot}
    Run Keyword If  '${txtrobot}'=='ROBOT FRAME WORK/'  Log To Console    Step is Pass
    ...    ELSE    Log To Console    Step is False
    Close Browser


TC05
    Open Browser    http://advantageonlineshopping.com/#/category/Mice/5    gc
    sleep    10
    ${txtrobot}    Get Text    //*[@data-ng-class="{ noPromotedProduct : viewAll }"]
    Log To Console    ${txtrobot}


TC06
    Open Browser    http://advantageonlineshopping.com/#/product/16    gc
    sleep    10
    Click Element     //*[@class="plus"]

    2x2

